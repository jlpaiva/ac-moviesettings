package com.avenuecode.jonatas.moviesettings;

import java.io.File;
import java.nio.file.Files;
import java.util.List;

import junit.framework.TestCase;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import com.avenuecode.jonatas.moviesettings.entity.FilmCharacter;
import com.avenuecode.jonatas.moviesettings.entity.Setting;

/**
 * Unit tests for this application.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = App.class)
@WebIntegrationTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AppTest extends TestCase {

	private static String LOCALHOST = "http://localhost:8080";

	RestTemplate template = new TestRestTemplate();

	/**
	 * Tests for parsing the script and insertting it into the database.
	 * */
	@Test
	public void test1() throws Exception {

		// Read script
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("files/test.txt")
				.getFile());
		List<String> input = Files.readAllLines(file.toPath());
		String script = "";

		for (String s : input) {
			script += s + System.getProperty("line.separator");
		}

		// First test, must be ok
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.TEXT_PLAIN);
		HttpEntity<String> entity = new HttpEntity<String>(script, headers);

		String url = LOCALHOST + "/script";

		ResponseEntity<String> response = template.exchange(url,
				HttpMethod.POST, entity, String.class);

		assertEquals(response.getStatusCode(), HttpStatus.OK);

		// Second test, must fail
		response = template
				.exchange(url, HttpMethod.POST, entity, String.class);

		assertEquals(response.getStatusCode(), HttpStatus.FORBIDDEN);
	}

	/**
	 * Tests for retrieving Settings. First all of them, afterwards a single one
	 * by an existing id and, finally, the last one by a non existing id.
	 * */
	@Test
	public void test2() throws Exception {
		String url = LOCALHOST + "/settings";
		ResponseEntity<List> response = template.getForEntity(url, List.class);
		assertEquals(response.getStatusCode(), HttpStatus.OK);

		url = LOCALHOST + "/settings/1";
		ResponseEntity<Setting> responseSetting = template.getForEntity(url,
				Setting.class);
		assertEquals(responseSetting.getStatusCode(), HttpStatus.OK);

		// Second test, must fail
		url = LOCALHOST + "/settings/100000000";
		responseSetting = template.getForEntity(url, Setting.class);
		assertEquals(responseSetting.getStatusCode(), HttpStatus.NOT_FOUND);
	}

	/**
	 * Tests for retrieving FilmCharacter. First all of them, afterwards a single one
	 * by an existing id and, finally, the last one by a non existing id.
	 * */
	@Test
	public void test3() throws Exception {
		String url = LOCALHOST + "/characters";
		ResponseEntity<List> response = template.getForEntity(url, List.class);
		assertEquals(response.getStatusCode(), HttpStatus.OK);

		url = LOCALHOST + "/characters/1";
		ResponseEntity<FilmCharacter> responseFilmChar = template.getForEntity(
				url, FilmCharacter.class);
		assertEquals(responseFilmChar.getStatusCode(), HttpStatus.OK);

		// Second test, must fail
		url = LOCALHOST + "/characters/100000000";
		responseFilmChar = template.getForEntity(url, FilmCharacter.class);
		assertEquals(responseFilmChar.getStatusCode(), HttpStatus.NOT_FOUND);
	}

}
