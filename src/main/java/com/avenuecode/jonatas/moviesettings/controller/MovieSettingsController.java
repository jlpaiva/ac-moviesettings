package com.avenuecode.jonatas.moviesettings.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.avenuecode.jonatas.moviesettings.entity.FilmCharacter;
import com.avenuecode.jonatas.moviesettings.entity.Setting;
import com.avenuecode.jonatas.moviesettings.exception.MovieSettingsException;
import com.avenuecode.jonatas.moviesettings.parse.Parser;
import com.avenuecode.jonatas.moviesettings.utils.HibernateUtils;
import com.avenuecode.jonatas.moviesettings.utils.SimpleResponse;

/**
 * Controller for all Rest webservices in this application
 * */
@RestController
public class MovieSettingsController {

	// Control variable
	private static boolean SCRIPT_ACCEPTED = false;

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * Treats all the requests for the '/script' service.
	 * 
	 * There's no check for invalid inputs (like null or empty). The input must
	 * come on the body of the request, not on the header.
	 * */
	@RequestMapping(value = "/script", method = RequestMethod.POST)
	public ResponseEntity<Object> script(@RequestBody String script) {

		// Only one script accepted per run
		if (SCRIPT_ACCEPTED) {
			log.error("Movie script already received");
			return new ResponseEntity<>(new SimpleResponse(
					"Movie script already received"), HttpStatus.FORBIDDEN);
		}

		// Parses and saves the entities
		try {
			Parser parser = new Parser(script);
			List<Setting> settings = parser.parse();
			HibernateUtils.saveList(settings);
		} catch (Exception e) {
			log.error("Unknow error: [Cause] -> " + e.getMessage());
			throw new MovieSettingsException("default", "Unknow error.");
		}

		SCRIPT_ACCEPTED = true;

		return new ResponseEntity<>(new SimpleResponse(
				"Movie script successfully received"), HttpStatus.OK);
	}

	/**
	 * Treats all the requests for the '/settings' service.
	 * 
	 * This service does not receive any inputs. It returns all elements in
	 * database.
	 * */
	@RequestMapping(value = "/settings", method = RequestMethod.GET)
	public ResponseEntity<Object> getAllSettings() {

		List<Setting> allSettings;

		try {
			allSettings = HibernateUtils.getAllSetting();
		} catch (Exception e) {
			log.error("Unknow error: [Cause] -> " + e.getMessage());
			throw new MovieSettingsException("default", "Unknow error.");
		}

		return new ResponseEntity<>(allSettings, HttpStatus.OK);
	}

	/**
	 * Treats all the requests for the '/settings/{id}' service.
	 * 
	 * This service receives the id of a Setting entity as parameter. It returns
	 * the element by that id or an error message.
	 * */
	@RequestMapping(value = "/settings/{id}", method = RequestMethod.GET)
	public ResponseEntity<Object> getSettingById(@PathVariable Long id) {

		Setting setting;

		try {
			setting = HibernateUtils.getSettingById(id);
		} catch (Exception e) {
			log.error("Unknow error: [Cause] -> " + e.getMessage());
			throw new MovieSettingsException("default", "Unknow error.");
		}

		if (setting == null) {
			log.error("Movie setting with id " + id + " not found.");
			return new ResponseEntity<>(new SimpleResponse(
					"Movie setting with id " + id + " not found."),
					HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(setting, HttpStatus.OK);
	}

	/**
	 * Treats all the requests for the '/characters' service.
	 * 
	 * This service does not receive any inputs. It returns all elements in
	 * database.
	 * */
	@RequestMapping(value = "/characters", method = RequestMethod.GET)
	public ResponseEntity<Object> getAllCharacters() {

		List<FilmCharacter> allFC;

		try {
			allFC = HibernateUtils.getAllFilmCharacter();
		} catch (Exception e) {
			log.error("Unknow error: [Cause] -> " + e.getMessage());
			throw new MovieSettingsException("default", "Unknow error.");
		}

		return new ResponseEntity<>(allFC, HttpStatus.OK);
	}

	/**
	 * Treats all the requests for the '/characters/{id}' service.
	 * 
	 * This service receives the id of a FilmCharacter entity as parameter. It
	 * returns the element by that id or an error message.
	 * */
	@RequestMapping(value = "/characters/{id}", method = RequestMethod.GET)
	public ResponseEntity<Object> getFilmCharacterById(@PathVariable Long id) {

		FilmCharacter filmCharacter;

		try {
			filmCharacter = HibernateUtils.getFilmCharacterById(id);
		} catch (Exception e) {
			log.error("Unknow error: [Cause] -> " + e.getMessage());
			throw new MovieSettingsException("default", "Unknow error.");
		}

		if (filmCharacter == null) {
			log.error("Movie character with id " + id + " not found.");
			return new ResponseEntity<>(new SimpleResponse(
					"Movie character with id " + id + " not found."),
					HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(filmCharacter, HttpStatus.OK);
	}

	/**
	 * Handles this application specific exception, in order to provide the
	 * response in the desired format.
	 * */
	@ExceptionHandler(MovieSettingsException.class)
	public @ResponseBody SimpleResponse handleError(HttpServletRequest req,
			MovieSettingsException ex) {
		return new SimpleResponse(ex.getMessage());
	}

}
