package com.avenuecode.jonatas.moviesettings.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * Entity that represents the Settings in a Script
 * */
@Entity
@Table(name = "SETTINGS")
public class Setting {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SETTING_ID")
	private Long id;
	
	private String name;

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(name = "SETTING_FILM_CHARACTER", 
				joinColumns = { @JoinColumn(name = "SETTING_ID") }, 
				inverseJoinColumns = { @JoinColumn(name = "FILM_CHARACTER_ID") })
	private Set<FilmCharacter> characters;

	public Setting() {
		super();
		characters = new HashSet<FilmCharacter>();
	}
	
	public Setting(String name) {
		super();
		this.name = name;
		characters = new HashSet<FilmCharacter>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<FilmCharacter> getCharacters() {
		return characters;
	}

	public void setCharacters(Set<FilmCharacter> characters) {
		this.characters = characters;
	}

	public void addCharacter(FilmCharacter filmChar) {
		if (!this.characters.contains(filmChar))
			this.characters.add(filmChar);
	}

}
