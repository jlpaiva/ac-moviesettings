package com.avenuecode.jonatas.moviesettings.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.avenuecode.jonatas.moviesettings.utils.ReverseWordCountComparator;

/**
 * Entity that represents a Character in the Script
 * */
@Entity
@Table(name = "FILM_CHARACTER")
public class FilmCharacter {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "FILM_CHARACTER_ID")
	private Long id;
	private String name;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(name = "FILM_CHARACTER_WORDS")
	private List<WordCount> words;

	public FilmCharacter() {
		this.words = new ArrayList<WordCount>();
	}

	public FilmCharacter(String name) {
		this.name = name;
		this.words = new ArrayList<WordCount>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<WordCount> getWords() {
		Collections.sort(this.words, new ReverseWordCountComparator());
		return this.words;
	}

	public void setWords(List<WordCount> words) {
		this.words = words;
	}

	public void incrementWord(String word) {
		WordCount wc = new WordCount(word, 1);

		int i = this.words.indexOf(wc);
		if (i == -1) {
			this.words.add(wc);
		} else {
			this.words.get(i).increaseCount();
		}
	}

}
