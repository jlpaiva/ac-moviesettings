package com.avenuecode.jonatas.moviesettings.parse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.avenuecode.jonatas.moviesettings.entity.FilmCharacter;
import com.avenuecode.jonatas.moviesettings.entity.Setting;
import com.avenuecode.jonatas.moviesettings.entity.WordCount;
import com.avenuecode.jonatas.moviesettings.utils.ReverseWordCountComparator;

/**
 * Class responsible for parsing the script and extract the settings and
 * characters lines.
 * */
public class Parser {

	private String[] scriptLines;

	public Parser(String script) {
		super();
		// creates an array separating by the default system separator
		this.scriptLines = script.split(System.getProperty("line.separator"));
	}

	public Parser(String[] scriptLines) {
		super();
		this.scriptLines = scriptLines;
	}

	public Parser(List<String> script) {
		super();
		this.scriptLines = script.toArray(new String[0]);
	}

	/**
	 * Method that performs the parsing of the Script
	 * */
	public List<Setting> parse() {
		List<Setting> settings = new ArrayList<>();
		Map<String, FilmCharacter> allChars = new HashMap<>();

		int i = 0;
		String currentLine = null;
		String currentSetting = null;

		// Performs this until the first setting in the script is found
		while (currentSetting == null && i < this.scriptLines.length) {
			currentLine = this.scriptLines[i++];

			if (currentLine.startsWith("INT./EXT.")) {
				currentSetting = currentLine.substring(9,
						currentLine.indexOf(" - ")).trim();
			} else if (currentLine.startsWith("EXT.")
					|| currentLine.startsWith("INT.")) {
				currentSetting = currentLine.substring(4,
						currentLine.indexOf(" - ")).trim();
			}
		}

		Setting setting = new Setting(currentSetting);
		settings.add(setting);

		FilmCharacter currentChar = null;

		// For the remaining lines
		while (i < this.scriptLines.length) {
			currentLine = this.scriptLines[i++];

			// identifies a new scene
			if (currentLine.startsWith("INT./EXT.")) {
				int lastIndex = currentLine.length();

				if (currentLine.contains(" - ")) {
					lastIndex = currentLine.indexOf(" - ");
				}

				currentSetting = currentLine.substring(9, lastIndex).trim();
				setting = new Setting(currentSetting);
				settings.add(setting);
				continue;
			}

			// identifies a new scene
			if (currentLine.startsWith("EXT.")
					|| currentLine.startsWith("INT.")) {
				int lastIndex = currentLine.length();

				if (currentLine.contains(" - ")) {
					lastIndex = currentLine.indexOf(" - ");
				}

				currentSetting = currentLine.substring(4, lastIndex).trim();
				setting = new Setting(currentSetting);
				settings.add(setting);
				continue;
			}

			// If there are more than 22 spaces the line is not important
			if (currentLine.startsWith("                       ")) {
				continue;
			}

			// line with 22 spaces
			if (currentLine.startsWith("                      ")) {
				String name = currentLine.trim();

				// Characters always with upper case letters (and spaces
				// sometimes)
				if (!name.matches("[A-Z ]+"))
					continue;

				// keeps a reference for the character in this map, so no new
				// characters are created (which would be wrong)
				if (allChars.containsKey(name)) {
					currentChar = allChars.get(name);
				} else {
					currentChar = new FilmCharacter(name);
					allChars.put(name, currentChar);
				}
				setting.addCharacter(currentChar);
				continue;
			}

			// line with 15 spaces is not important
			if (currentLine.startsWith("               ")) {
				continue;
			}

			// line with 10 spaces, means the lines of a character
			if (currentLine.startsWith("          ")) {
				String[] tokens = currentLine.trim().toLowerCase()
						.replaceAll("[^a-z0-9 ]", "").split(" ");
				for (String token : tokens) {
					currentChar.incrementWord(token);
				}

				continue;
			}

		}

		// Only the top 10 most used words are important for each character, no
		// need to store the rest
		for (String name : allChars.keySet()) {
			FilmCharacter fc = allChars.get(name);
			List<WordCount> words = fc.getWords();
			Collections.sort(words, new ReverseWordCountComparator());
			if (words.size() > 10) {
				fc.setWords(words.subList(0, 10));
			}
		}

		return settings;
	}

}
