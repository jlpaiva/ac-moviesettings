package com.avenuecode.jonatas.moviesettings.utils;

import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

/**
 * Class that handles the singleton SessionFactory object in the application.
 * */
public class HibernateSession {
	private static final SessionFactory sessionFactory;

	static {

		final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
				.configure().build();

		try {
			sessionFactory = new MetadataSources(registry).buildMetadata()
					.buildSessionFactory();
		} catch (Throwable ex) {
			StandardServiceRegistryBuilder.destroy(registry);
			System.err.println("Session Factory could not be created." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static void closeSessionFactory() {
		sessionFactory.close();
	}

}
