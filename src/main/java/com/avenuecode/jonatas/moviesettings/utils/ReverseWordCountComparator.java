package com.avenuecode.jonatas.moviesettings.utils;

import java.util.Comparator;

import com.avenuecode.jonatas.moviesettings.entity.WordCount;

/**
 * Helping class for compare two WordCount objects. It gives the reverse order.
 * */
public class ReverseWordCountComparator implements Comparator<WordCount> {

	@Override
	public int compare(WordCount o1, WordCount o2) {
		return (int) (o2.getCount() - o1.getCount());
	}

}
