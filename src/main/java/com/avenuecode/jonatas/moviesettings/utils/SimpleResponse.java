package com.avenuecode.jonatas.moviesettings.utils;

/**
 * Simple structure for wrapping a simple message.
 * */
public class SimpleResponse {

	private String message;

	public SimpleResponse() {
		super();
	}

	public SimpleResponse(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
