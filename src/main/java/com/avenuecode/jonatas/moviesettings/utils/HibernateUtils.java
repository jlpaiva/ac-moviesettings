package com.avenuecode.jonatas.moviesettings.utils;

import java.util.List;

import org.hibernate.Session;

import com.avenuecode.jonatas.moviesettings.entity.FilmCharacter;
import com.avenuecode.jonatas.moviesettings.entity.Setting;

/**
 * Class with the persistence methods
 * */
public class HibernateUtils {

	/**
	 * Saves any object
	 * */
	public static void saveObject(Object obj) {
		Session session = HibernateSession.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(obj);
		session.getTransaction().commit();
		session.close();
	}

	/**
	 * Saves a list of settings
	 * */
	public static void saveList(List<Setting> list) {
		Session session = HibernateSession.getSessionFactory().openSession();
		session.beginTransaction();
		for (Setting setting : list)
			session.save(setting);
		session.getTransaction().commit();
		session.close();
	}

	/**
	 * Retrieves a single FilmCharacter by its id
	 * */
	public static FilmCharacter getFilmCharacterById(Long id) {

		Session session = HibernateSession.getSessionFactory().openSession();
		session.beginTransaction();
		FilmCharacter filmChar = session.get(FilmCharacter.class, id);
		session.getTransaction().commit();

		return filmChar;
	}

	/**
	 * Retrieves all FilmCharacters
	 * */
	public static List<FilmCharacter> getAllFilmCharacter() {

		Session session = HibernateSession.getSessionFactory().openSession();
		session.beginTransaction();
		List<FilmCharacter> filmChars = session.createCriteria(
				FilmCharacter.class).list();
		session.getTransaction().commit();
		return filmChars;
	}

	/**
	 * Retrieves a single Setting by its id
	 * */
	public static Setting getSettingById(Long id) {
		Session session = HibernateSession.getSessionFactory().openSession();
		session.beginTransaction();
		Setting setting = session.get(Setting.class, id);
		session.getTransaction().commit();

		return setting;
	}

	/**
	 * Retrieves all Settings
	 * */
	public static List<Setting> getAllSetting() {
		Session session = HibernateSession.getSessionFactory().openSession();
		session.beginTransaction();
		List<Setting> settings = session.createCriteria(Setting.class).list();
		session.getTransaction().commit();
		return settings;
	}

}
