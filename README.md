Na pasta raiz

Para compilar

**mvn compile**

Para criar um pacote

**mvn package**

Para instalar num repositório local

**mvn install**

Para rodar a aplicação

**mvn spring-boot:run**

Para executar os testes (**definidos em com.avenuecode.jonatas.moviesettings.AppTest.java**)

**mvn test**